#include "pkdialplugin.h"
#include "pkdial.h"

#include <QtPlugin>

PKDialPlugin::PKDialPlugin(QObject *parent) : QObject(parent) {
	m_initialized = false;
}

void PKDialPlugin::initialize(QDesignerFormEditorInterface * /* core */) {
	if (m_initialized)
		return;

	// Add extension registrations, etc. here

	m_initialized = true;
}

bool PKDialPlugin::isInitialized() const { return m_initialized; }

QWidget *PKDialPlugin::createWidget(QWidget *parent) {
	return new PKDial(parent);
}

QString PKDialPlugin::name() const { return QLatin1String("PKDial"); }

QString PKDialPlugin::group() const { return QLatin1String("PKWidgets"); }

QIcon PKDialPlugin::icon() const { return QIcon(":/PKDial.xpm"); }

QString PKDialPlugin::toolTip() const {
	return QLatin1String("Create a configurable dial");
}

QString PKDialPlugin::whatsThis() const {
	return QLatin1String(
		"A dial with inside markers and the possibility to add a"
		"calculater\n\t");
}

bool PKDialPlugin::isContainer() const { return false; }

QString PKDialPlugin::domXml() const {
	return QLatin1String(
		"<widget class=\"PKDial\" name=\"pKDial\">\n</widget>\n");
}

QString PKDialPlugin::includeFile() const { return QLatin1String("pkdial.h"); }
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pkdialplugin, PKDialPlugin)
#endif // QT_VERSION < 0x050000
