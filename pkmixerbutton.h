#ifndef PKMIXERBUTTON_H
#define PKMIXERBUTTON_H

#include <QObject>
#include <QToolButton>

class PKMixerButton : public QToolButton {
	Q_OBJECT

	Q_PROPERTY(QColor ActiveColor READ activeColor WRITE setActiveColor)

  public:
	PKMixerButton(QWidget *parent = nullptr);

  public: // getter
	QColor activeColor();

  public: // setter
	void setActiveColor(QColor color);

  private:
	QColor m_ColorActive = QColor(0, 255, 0);

	void init();
};

#endif // PKMIXERBUTTON_H
