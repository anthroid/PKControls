#ifndef PKLEVELMEMETER_H
#define PKLEVELMEMETER_H

#include "calc/scalecalc.h"

#include <memory>

#include <QColor>
#include <QMouseEvent>
#include <QTimer>
#include <QWidget>

class PKLevelMeter : public QWidget {
	Q_OBJECT

	Q_PROPERTY(double levelMin READ levelMin WRITE setLevelMin)
	Q_PROPERTY(double levelMax READ levelMax WRITE setLevelMax)
	Q_PROPERTY(double level READ level WRITE setLevel)
	Q_PROPERTY(double levelWarning READ levelWarning WRITE setLevelWarning)
	Q_PROPERTY(double levelCritical READ levelCritical WRITE setLevelCritical)
	Q_PROPERTY(
		bool showWarningColor READ showWarningColor WRITE setShowWarningColor)
	Q_PROPERTY(bool showCriticalColor READ showCriticalColor WRITE
				   setShowCriticalColor)
	Q_PROPERTY(QColor colorCritical READ colorCritical WRITE setColorCritical)
	Q_PROPERTY(QColor colorWarning READ colorWarning WRITE setColorWarning)
	Q_PROPERTY(QColor colorPlain READ colorPlain WRITE setColorPlain)
	Q_PROPERTY(bool showOverloadSignal READ showOverloadSignal WRITE
				   setShowOverloadSignal)
	Q_PROPERTY(bool holdMaxValue READ holdMaxValue WRITE setHoldMaxValue)

  public:
	PKLevelMeter(QWidget *parent = nullptr);
	~PKLevelMeter() override;

	void setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc);

	void reset();

	float levelMin() const;
	float levelMax() const;
	float level() const;

	float levelWarning() const;
	void setLevelWarning(float levelWarning);

	float levelCritical() const;
	void setLevelCritical(float levelCritical);

	bool showWarningColor() const;
	void setShowWarningColor(bool showWarningColor);

	bool showCriticalColor() const;
	void setShowCriticalColor(bool showCriticalColor);

	QColor colorCritical() const;
	void setColorCritical(const QColor &colorCritical);

	QColor colorWarning() const;
	void setColorWarning(const QColor &colorWarning);

	QColor colorPlain() const;
	void setColorPlain(const QColor &colorPlain);

	bool showOverloadSignal() const;
	void setShowOverloadSignal(bool showOverloadSignal);

	bool holdMaxValue() const;
	void setHoldMaxValue(bool holdMaxValue);

  public slots:
	void setLevelMin(float levelMin);
	void setLevelMax(float levelMax);
	void setLevel(float level);

	// QWidget interface
  protected:
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void mousePressEvent(QMouseEvent *event) override;

  private:
	void paintSection(QPainter &painter, QColor &color, float fromVal,
					  float toVal);

	void initValues();

	int calcPosition(float level);

	float m_fLevel = 20;
	float m_fLevelMin = 0;
	float m_fLevelMax = 100;

	float m_fRange;

	float m_fLevelWarning = 70;
	float m_fLevelCritical = 95;

	float m_fCurrentMax;

	bool m_bShowOverloadSignal = false;
	bool m_bOverload = false;

	bool m_bShowWarningColor = false;
	bool m_bShowCriticalColor = false;

	bool m_bHoldMaxValue = false;

	QColor m_ColorCritical = QColor(255, 32, 16);
	QColor m_ColorWarning = QColor(255, 250, 79);
	QColor m_ColorPlain = QColor(71, 180, 31);

	QRect m_RectContent;
	QRect m_RectMeter;
	QRect m_RectInnerMeter;
	QRect m_RectOverload;
	QRect m_RectInnerOverload;

	QTimer m_timerHold;

	std::shared_ptr<ScaleCalc> m_pScaleCalc = nullptr;
};

#endif // PKLEVELMEMETER_H
