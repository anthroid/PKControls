#ifndef PKDIALPLUGIN_H
#define PKDIALPLUGIN_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>

class PKDialPlugin : public QObject, public QDesignerCustomWidgetInterface {
	Q_OBJECT
	Q_INTERFACES(QDesignerCustomWidgetInterface)

  public:
	PKDialPlugin(QObject *parent = nullptr);

	bool isContainer() const;
	bool isInitialized() const;
	QIcon icon() const;
	QString domXml() const;
	QString group() const;
	QString includeFile() const;
	QString name() const;
	QString toolTip() const;
	QString whatsThis() const;
	QWidget *createWidget(QWidget *parent);
	void initialize(QDesignerFormEditorInterface *core);

  private:
	bool m_initialized;
};

#endif // PKDIALPLUGIN_H
