#ifndef PKLEVELMETERPLUGIN_H
#define PKLEVELMETERPLUGIN_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>

class PKLevelMeterPlugin : public QObject,
						   public QDesignerCustomWidgetInterface {
	Q_OBJECT
	Q_INTERFACES(QDesignerCustomWidgetInterface)

  public:
	PKLevelMeterPlugin(QObject *parent = nullptr);

	bool isContainer() const;
	bool isInitialized() const;
	QIcon icon() const;
	QString domXml() const;
	QString group() const;
	QString includeFile() const;
	QString name() const;
	QString toolTip() const;
	QString whatsThis() const;
	QWidget *createWidget(QWidget *parent);
	void initialize(QDesignerFormEditorInterface *core);

  private:
	bool m_initialized;
};

#endif // PKLEVELMETERPLUGIN_H
